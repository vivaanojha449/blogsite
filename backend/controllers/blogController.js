
import Blog from "../models/blogModel.js";

/**
 * @desc		Get all blogs
 * @router	GET /api/blogs
 * @access	public
 */

const getBlogs= async (req, res) => {
	const blogs = await Blog.find({});
	res.json(blogs);
};

/**
 * @desc		Get single blogs
 * @router	GET /api/blogs/:id
 * @access	public
 */
const getBlogById = async (req, res) => {
	const blog = await Blog.findById(req.params.id);

	if (blog) {
		res.json(blog);
	} else {
		res.status(404);
		throw new Error('Blog not found');
	}
};

/**
 * @desc		Delete a blog
 * @router	DELETE /api/blogs/:id
 * @access	private/admin
 */
const deleteblog = async (req, res) => {
	const blog = await Blog.findById(req.params.id);

	if (blog) {
		await Blog.deleteOne(blog);
		res.json({ message: 'Blog deleted' });
	} else {
		res.status(404);
		throw new Error('Blog not found');
	}
};


const createBlog = async (req, res) => {
	const blog = new Blog({
		user: req.user._id,
		title: 'Building microservices with Dropwizard, MongoDB & Docker',
		short_excerpt: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, magni! Earum sunt quis voluptatem provident consequatur totam odio sint architecto repellendus.',
		image: '/images/sample.jpg',
		author: 'John Doe',
		date: 'MM DD, YY',
		content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime, magni! Earum sunt quis voluptatem provident consequatur totam odio sint architecto repellendus. Possimus fugit eius dolores. Magnam hic, in ad laborum soluta, explicabo, at quas distinctio nihil et ipsam dolores labore praesentium veniam ducimus qui rem. Autem voluptate nihil eaque sapiente.',
	});

	const createdBlog = await blog.save();
	res.status(201).json(createdBlog);
};

const updateBlog = async (req, res) => {
	const { title, short_excerpt, image, author, date, content } =
		req.body;

	const blog = await Blog.findById(req.params.id);

	if (blog) {
		blog.title = title;
		blog.short_excerpt = short_excerpt;
		blog.image = image;
		blog.author = author;
		blog.date = date;
		blog.content = content;

		const updatedBlog = await blog.save();
		res.json(updatedBlog);
	} else {
		res.status(404);
		throw new Error('Blog not found');
	}
};	


/**
 * @desc		Create new review
 * @router	POST /api/blogs/:id/comments
 * @access	private
 */
const createBlogComment = async (req, res) => {
    const { comment } = req.body;

    try {
        const blog = await Blog.findById(req.params.id);

        if (!blog) {
            res.status(404).json({ message: 'Blog not found' });
            return;
        }

        const alreadyCommented = blog.comments.find	(
            (c) => c.user.toString() === req.user._id.toString()
        );

        if (alreadyCommented) {
            res.status(400).json({ message: 'Blog already commented' });
            return;
        }

        const userComment = {
            name: req.user.name,
            comment,
            user: req.user._id,
        };

        blog.comments.push(userComment);
        await blog.save();

        res.status(201).json({ message: 'Comment added', comment: userComment });
    } catch (error) {
        console.error(error.message);
        res.status(500).json({ message: 'Server error' });
    }
};


export { getBlogs, getBlogById, deleteblog, createBlog, updateBlog, createBlogComment }