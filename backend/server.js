import colors from 'colors';
import dotenv from 'dotenv';
import express from 'express';
import connectDB from './config/db.js';
import { errorHandler } from './middlewares/errorMiddleware.js';
import userRoutes from './routes/userRoutes.js';
import blogRoutes from './routes/blogRoutes.js'

dotenv.config();

connectDB();

const app = express();
app.use(express.json()); // Request Body Parsing

app.get('/', (req, res) => {
	res.send('API is running...');
});

app.use('/api/blogs', blogRoutes);
app.use('/api/users', userRoutes);

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
	console.log(
		`Server running in ${process.env.NODE_ENV} mode, on port ${PORT}.`.yellow
			.bold
	);
});
